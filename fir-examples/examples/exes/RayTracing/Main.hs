module Main where

import FIR.Examples.RayTracing.Application
  ( rayTracing )

main :: IO ()
main = rayTracing
